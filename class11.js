"use strict";

function setup(e){

    let counter = 0;
    const form = document.getElementsByTagName("form")[counter];

    function changeColor(e){
        e.preventDefault();

        let color = `rgb(${e.target[`Red${counter}`].value},${e.target[`Green${counter}`].value},${e.target[`Blue${counter}`].value})`;

        const fieldset = document.getElementsByTagName("fieldset")[counter];
        
        fieldset.style.backgroundColor = `${color}`;
    }
    form.addEventListener("submit", changeColor);

    const button = document.getElementById("clonebutton");
    function cloneForm(){

        counter++;
        console.log(counter);
        const dupForm = document.createElement("form");
        const demoDiv = document.getElementById("demo");
        demoDiv.insertBefore(dupForm, button);

        const dupFieldset = document.createElement("fieldset");
        dupForm.appendChild(dupFieldset);
        dupFieldset.classList.add("style");

        const colors = ["Red", "Green", "Blue"];        

        for(let i = 0; i < 3; i++){
            const divs = document.createElement("div");
            const label = document.createElement("label");
            const input = document.createElement("input");

            dupFieldset.appendChild(divs);
 
            divs.appendChild(label);
            label.textContent = `${colors[i]}: `;
            label.setAttribute("for", `${colors[i]}${counter}`);
 
            divs.appendChild(input);
            input.setAttribute("type", "range");
            input.setAttribute("name", `${colors[i]}${counter}`);
            input.setAttribute("id", `${colors[i]}${counter}`);
            input.setAttribute("min", "0");
            input.setAttribute("max", "255");
            input.setAttribute("step", "1");
        }

        const newButton = document.createElement("button");
        dupFieldset.appendChild(newButton);
        newButton.textContent = "Submit";

        dupForm.addEventListener("submit", changeColor);
        console.log("duplicate form: " + dupForm);
    }

    button.addEventListener("click", cloneForm);
    

}

document.addEventListener("DOMContentLoaded", setup);